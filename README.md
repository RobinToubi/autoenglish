# Auto english

Le but de cette application est de faire automatiquement (de la manière la plus basique) les différents cours d'anglais de la plateforme global-exam.
Cette application est à but totalement expérimental et ne doit en aucun cas être utilisé à défunt de triche.

# Installation

Créer un environnement virtuel : 
```py
python3 -m venv my_venv
```

Activer l'environnement : 
```py
. venv/bin/activate
```

Installer les packages : 
```py
pip3 install -r ./requirements.txt
```

En fonction du driver que vous utilisez, vous allez devoir l'installer :

https://selenium-python.readthedocs.io/installation.html

Driver| Lien de téléchargement
--|--
Firefox | https://github.com/mozilla/geckodriver/releases
Chrome | https://chromedriver.storage.googleapis.com/index.html?path=97.0.4692.71/
Safari | https://webkit.org/blog/6900/webdriver-support-in-safari-10/

N'oubliez pas d'update votre PATH pour rendre les driver executables.

# Mise en place

Vous devez commencer par mettre vos credentials (email et mot de passe) dans le fichier `env.py`.

# Aide

Ensuite vous pouvez lancer l'application avec les aides en lancant la commande suivante : `python main.py -h`.
Plusieurs commandes sont disponibles :
- `--driver` : choisir le driver que vous souhaitez utiliser (Safari (par défaut), Firefox ou Chrome)
- `--onlyhours` : faire uniquement les session pour effectuer ses 3 heures de travail
