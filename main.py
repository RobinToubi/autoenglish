import time, env, argparse
from selenium import webdriver
from selenium.webdriver.common.by import By

parser = argparse.ArgumentParser()
parser.add_argument('--driver', '-d', help='Select the driver used by the program', type=str, default="Safari")
parser.add_argument('--onlyhours', '-oh', help='Launch only sessions to make 3 hours activity', type=bool, default=False)

args = parser.parse_args()

# If you want to use another answer list
# you just need to copy all XPATH of answer's labels in this list
# Screen's here : https://i.stack.imgur.com/JyFAM.png
SESSION_RESPONSES = [
    # Réponse A
    "/html/body/div[1]/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[2]/div/label[1]",
    # Réponse D
    "/html/body/div[1]/div/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div/label[4]",
    # Réponse A
    "/html/body/div[1]/div/div[2]/div[2]/div[2]/div[3]/div[2]/div[2]/div/label[1]",
    # Réponse C
    "/html/body/div[1]/div/div[2]/div[2]/div[2]/div[4]/div[2]/div[2]/div/label[3]",
    # Réponse B
    "/html/body/div[1]/div/div[2]/div[2]/div[2]/div[5]/div[2]/div[2]/div/label[2]",
    # Réponse B
    "/html/body/div[1]/div/div[2]/div[2]/div[2]/div[6]/div[2]/div[2]/div/label[2]"
]

# Define used driver given in first argument
def define_driver(driver):
    if driver == "Safari":
        driver = webdriver.Safari()
    elif driver == "Firefox":
        driver = webdriver.Firefox()
    else:
        driver = webdriver.Chrome()
    driver.maximize_window()
    return driver

def login():
    driver.get("https://auth.global-exam.com/sso/cas/ynov/4604")
    time.sleep(3)
    username_input = driver.find_element(By.ID, "username")
    password_input = driver.find_element(By.ID, "password")
    login_button = driver.find_element(
        By.XPATH, "/html/body/main/div/div/div[2]/form/div[3]/div/input[4]")
    username_input.send_keys(env.YMAIL)
    password_input.send_keys(env.YPASSWORD)
    login_button.click()
    time.sleep(10)


def session(timer = 0):
    for element in SESSION_RESPONSES:
        answer = driver.find_element(By.XPATH, element)
        answer.click()
        time.sleep(1)
    time.sleep(timer)
    send_button = driver.find_element(
        By.XPATH, "/html/body/div[1]/div/div[3]/div/button")
    send_button.click()
    time.sleep(3)
    return

def make_session(iteration = 10, time_between_iteration = 0):
    for _ in range(iteration):
        replay_button = driver.find_element(
            By.XPATH, "/html/body/div[1]/div/div[3]/div[1]/div/a")
        replay_button.click()
        time.sleep(2)
        session(time_between_iteration)
        time.sleep(5)

driver = define_driver(args.driver) # This is just set for visibility issues
print(args.onlyhours)

try:
    # Authentication
    login()

    # Hide help modals
    driver.execute_script("localStorage.setItem('hideGuidelines', 'true')")

    # Go to mon parcours
    my_parcours_button = driver.find_element(
        By.XPATH, "/html/body/div[1]/main/div/div/div/div[1]/div[1]/div[2]/div/a")
    my_parcours_button.click()
    time.sleep(2)

    # Select course
    course_button = driver.find_element(
        By.XPATH, "/html/body/div[1]/main/div/div/div/div[2]/div[1]/div[2]/button[2]")
    course_button.click()
    time.sleep(3)

    if args.onlyhours != None:
        make_session(6, 1800)
    else:
        make_session(4)
        make_session(6, 1800)
finally:
    driver.quit()
